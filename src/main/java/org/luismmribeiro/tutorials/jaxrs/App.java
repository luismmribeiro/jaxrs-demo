package org.luismmribeiro.tutorials.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application configuration.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@ApplicationPath("/api")
public class App extends Application {

	@Override
	public Set<Class<?>> getClasses() {
	    final Set<Class<?>> classes = new HashSet<>();
	    
	    classes.add(CustomerService.class);
	    return classes;
	}
}
