package org.luismmribeiro.tutorials.jaxrs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;

/**
 * Simple service implementation.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Path("customers")
public class CustomerService {

	private static Map<Long, Customer> customers = new LinkedHashMap<>();

	@Context
	private UriInfo context;

	@GET
	@Path("healthCheck")
	@Produces(MediaType.TEXT_PLAIN)
	public String healthCheck() {
		return "URI " + context.getRequestUri().toString() + " is OK!";
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> listAllCustomers() {
		List<Customer> customers = new ArrayList<Customer>(CustomerService.customers.values());
		return customers;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public long createCustomer(Customer customer) {
		customer.setId(customers.size());
		customer.setLastChanged(new Date());
		customers.put(customer.getId(), customer);
		return customer.getId();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public long formCreateCustomer(@NotNull @FormParam("firstName") String firstName,
			@NotNull @FormParam("lastName") String lastName,
			@NotNull @FormParam("city") String city) {
		Customer customer = new Customer();
		customer.setCity(city);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		
		return createCustomer(customer);
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Customer updateCustomer(@PathParam("id") long id, Customer customer) {
		customers.put(id, customer);
		customer.setLastChanged(new Date());
		return customer;
	}

	@DELETE
	@Path("/{id}")
	public void deleteCustomer(@PathParam("id") long id) {
		customers.remove(id);
	}

	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer retrieveCustomerById(@PathParam("id") long id) {
		return customers.get(id);
	}

	@GET
	@Path("filter")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Customer> getFilterCustomers(@QueryParam("firstName") String firstName,
			@QueryParam("lastName") String lastName,
			@DefaultValue("Lisboa") @QueryParam("city") String city,
			@NotNull @Min(0) @QueryParam("page") int page,
			@NotNull @Min(2) @Max(10) @QueryParam("pageSize") int pageSize) {
		List<Customer> result = 
				customers.values().stream().filter(customer -> 
					((StringUtils.isBlank(firstName) || StringUtils.equals(customer.getFirstName(), firstName))) &&
					((StringUtils.isBlank(lastName) || StringUtils.equals(customer.getLastName(), lastName))) &&
					StringUtils.equals(customer.getCity(), city)).collect(Collectors.toList());

		int fromIndex = page * pageSize;
		int toIndex = fromIndex + pageSize;
		int resultSize = result.size();
		
		if(fromIndex >= resultSize) {
			result = Collections.emptyList();
		}
		else if(toIndex > resultSize) {
			result = result.subList(fromIndex, resultSize);
		}
		else {
			result = result.subList(fromIndex, toIndex);
		}
		return result;
	}
}