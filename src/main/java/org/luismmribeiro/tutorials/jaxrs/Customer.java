package org.luismmribeiro.tutorials.jaxrs;

import java.io.Serializable;
import java.util.Date;

/**
 * Simple customer entity
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String firstName;
	private String lastName;
	private String city;
	private Date lastChanged;

	public Customer() {}

	public Customer(String firstName, String lastName, String city) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.lastChanged = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(Date lastChanged) {
		this.lastChanged = lastChanged;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", city=" + city
				+ ", lastChanged=" + lastChanged + "]";
	}
}