# jaxrs-demo

This project was created to provide a small tutorial for JAX-RS.

The code was developed by [Luis Ribeiro](mailto:luismmribeiro@gmail.com) and it provides no warranties what so ever.

In order to correctly and automatically deploy the code, you should have a Wildfly 13.0 instance running on your localhost:8080. If this is the case, you can simply execute "mvn clean install" on your project root and the WAR will be deployed on your server. Otherwise, please remove the wildfly plugin from the pom.xml file.  